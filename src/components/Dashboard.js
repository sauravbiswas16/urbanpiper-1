import React from 'react';
import Navbar from './Navbar';
import axios from 'axios';
import setAuthToken from '../utils/setAuthToken';
import { Link } from 'react-router-dom';
class Dashboard extends React.Component {
    state = { tasks: [] };

    componentDidMount() {
        setAuthToken(localStorage.getItem('jwtToken'));
        axios
            .get('/tasks_list')
            .then(res => {

                this.setState({ tasks: res.data });
                console.log(this.state.tasks);
            })
            .catch(err =>
                console.log(err)
            );
    }

    renderDate(created_at){
        
        let date = new Date(created_at);
        return date.getFullYear()+'-' + (date.getMonth()+1) + '-'+date.getDate();
    }

    cancelTask(task_id){
        setAuthToken(localStorage.getItem('jwtToken'));
        axios
            .patch(`/tasks_update/${task_id}`,{status:'cancelled'})
            .then(res => {
                console.log(res.data);
            })
            .catch(err =>
                console.log(err)
            );
    }
    renderList() {
        return this.state.tasks.map((task) => {
            return (
                <tr key={task.id}>
                    <td>{task.title}</td>
                    <td>{task.priority.toString().toUpperCase()}</td>
                    <td>{task.status.toString().toUpperCase()}</td>
                    {/* <td>{task.created_at}</td>
                    <td>{task.created_by.username}</td> */}
                    <td>{task.deliver_person ? task.deliver_person : 'Not Accepted Yet'}</td>
                    <td>
                        <Link to={`/edit_task/${task.id}`} className="btn btn-info  col-md-6">Edit</Link>
                        <button type="button" className="btn btn-danger  col-md-6" onClick={this.cancelTask.bind(this,task.id)}>Cancel</button>
                    
                    </td>
                </tr>
            );
        });
    }

    render() {

        return (
            <div>
                <Navbar />

                <table className="table">
                    <thead className="thead-dark">
                        <tr>
                            <th scope="col">Task Title</th>
                            <th scope="col">Priority</th>
                            <th scope="col">Status</th>
                            <th scope="col">Delivery Person</th>
                            <th scope="col">Action</th>
                           
                        </tr>
                    </thead>
                    <tbody>
                        {this.renderList()}

                    </tbody>
                </table>


            </div>
        );
    }

}


export default Dashboard;