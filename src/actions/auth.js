import axios from 'axios';
import setAuthToken from '../utils/setAuthToken';
import jwt_decode from 'jwt-decode';

// Login - Get User Token
export const loginUser = userData => dispatch => {
  axios
    .post('http://localhost:8000/login', userData)
    .then(res => {
      // Save to localStorage
      const { token } = res.data;
      // Set token to ls
      localStorage.setItem('jwtToken', token);
      // Set token to Auth header
      setAuthToken(token);
    
      dispatch(setCurrentUser(res.data.user_id));
    })
    .catch(err =>
      dispatch({
        type: 'GET_ERRORS',
        payload: err
      })
    );
};

// Set logged in user
export const setCurrentUser = user_id => {
  return {
    type: 'SET_CURRENT_USER',
    payload: user_id
  };
};

// Log user out
export const logoutUser = () => dispatch => {
  // Remove token from localStorage
  localStorage.removeItem('jwtToken');
  // Remove auth header for future requests
  setAuthToken(false);
  // Set current user to {} which will set isAuthenticated to false
  dispatch(setCurrentUser({}));
};